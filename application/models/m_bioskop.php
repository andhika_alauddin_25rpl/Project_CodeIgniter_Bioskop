<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_bioskop extends CI_Model {

	public function tampil_film()
		{
			$tm_film = $this->db->get('film')->result();
			return $tm_film;
		}
	public function detail($a)
	{
		$tm_film=$this->db
					  ->where('id_film',$a)
					  ->get('film')
					  ->row();
		return $tm_film;
	}

}

/* End of file m_bioskop.php */
/* Location: ./application/models/m_bioskop.php */