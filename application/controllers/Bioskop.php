<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bioskop extends CI_Controller {

	public function index()
	{
		$data['judul']="Home";
		$this->load->view('Home',$data);		
	}
	public function login()
	{
		$data['judul']="Login";
		$this->load->view('login', $data);
	}
	public function register()
	{
		$data['judul']="Daftar";
		$this->load->view('reg',$data);
	}
	public function play()
	{
		$data['judul']="Now Playing";
		$data['konten'] = "play";
		$this->load->model('M_bioskop');
		$data['film'] = $this->M_bioskop->tampil_film();
		$this->load->view('header', $data);
	}
	public function transaksi()
	{
		$data['judul']="Book Ticket";
		$data['konten']="transaksi";
		$this->load->view('header', $data);
	}
	public function kritik()
	{
		$data['judul']="Kritik dan Saran";
		$data['konten']="kritik";
		$this->load->view('header', $data);
	}
	public function film()
	{
		$this->load->model('M_bioskop');
		$data['tampil_buku']=$this->M_bioskop->tampil_film();
		$data['konten'] = "film";
		$data['judul'] = "Daftar Film";
		$this->load->view('header', $data);	
	}
	public function detail_film($id_film)
	{
		$this->load->model('M_bioskop');
		$data['detail']=$this->M_bioskop->detail($id_film);
		$data['konten'] = "detail_film";
		$data['judul'] = "Detail Film";
		$this->load->view('header', $data);
	}
		public function simpan()
	{
		$nama=$this->input->post('name');
		$email=$this->input->post('email');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$retype=$this->input->post('retype');
		if ($password==$retype) {
		$data_simpan=array('nama' =>$nama ,
							'email'=>$email,
							'username'=>$username,
							'password'=>$password );
		$proses=$this->db->insert('pembeli', $data_simpan);
	}
	else{
			$this->session->set_flashdata('pesan','password tidak sama');
			redirect('bioskop/register','refresh');
	}
		if ($proses) {
			$this->session->set_flashdata('pesan','sukses');
			redirect('bioskop/login','refresh');

		}
		else{
			$this->session->set_flashdata('pesan','gagal');
			redirect('bioskop/register','refresh');
		}
	
	}


}

/* End of file Bioskop.php */
/* Location: ./application/controllers/Bioskop.php */