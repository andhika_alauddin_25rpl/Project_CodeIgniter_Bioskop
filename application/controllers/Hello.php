<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hello extends CI_Controller {

	public function index()
	{
		$data['judul']="Home";
		$data['nama']="Subejo";
		$this->load->view('hello_world',$data);

	}

}

/* End of file Hello.php */
/* Location: ./application/controllers/Hello.php */ 