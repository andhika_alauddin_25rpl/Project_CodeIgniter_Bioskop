  <!DOCTYPE html>
  <html>
  <head>
  	<title><?= $judul?></title>
  	<link rel="stylesheet" type="text/css" href="<?= base_url()?>asset/bootstrap-3.3.7-dist/css/bootstrap.css">
  	<link rel="stylesheet" type="text/css" href="<?= base_url()?>asset/style.css">
    <link rel="icon" type="img" href="<?= base_url()?>asset/jadi/icon.png">
  	<script type="text/javascript" src="<?= base_url()?>asset/jquery-ui-1.12.1/external/jquery/jquery.js"></script>
  	<script type="text/javascript" src="<?= base_url()?>asset/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>

    <nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
     <div class="navbar-header">

       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
       </button>
       <a class="navbar-brand" href="<?= base_url()?>index.php/Bioskop/index"><img src="<?= base_url()?>asset/jadi/logo.png"></a>
     </div>
     <div class="container-fluid">
       <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <ul class="nav navbar-nav">
           <li><a href="<?= base_url()?>index.php/Bioskop/play">Now Playing</a></li>
           <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm">About Us</a></li>
         </ul>
         <ul class="nav navbar-nav navbar-right">
          <li><a href="<?= base_url()?>index.php/Bioskop/login"><span class="glyphicon glyphicon-log-in">&nbsp</span>Login</a></li>
          <li><a href="<?= base_url()?>index.php/Bioskop/register">Daftar</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="header">
    <h1>Enjoy newest movie here.<br>Taste best experience watch movie with us.</h1>
  </div>
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="z-index: 1;">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      <li data-target="#carousel-example-generic" data-slide-to="3"></li>
      <li data-target="#carousel-example-generic" data-slide-to="4"></li>
      <li data-target="#carousel-example-generic" data-slide-to="5"></li>
      <li data-target="#carousel-example-generic" data-slide-to="6"></li>
      <li data-target="#carousel-example-generic" data-slide-to="7"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <div class="jumbotron slider" style="background-image: url(<?= base_url()?>asset/Jadi/bioskop1.png); background-size: cover; background-position: center;">
          <div class="container">
            <h1 style="color: white;">Blade Runner 2049</h1>
            <p style="font-size: 14px; color: white; padding-right: 40%;">Film Blade Runner 2049  terbaru Hollywood ini akan mengisahkan kisah tentang seorang mantan polisi yang bernama Rick Deckard, Yang mana Ia telah ditangkap oleh seorang polisi bernama Gaff, dan kemudian dia dibawa untuk diproses ke atasannya yaitu Bryant.
              <p><a class="btn btn-primary btn-lg" role="button">Lihat Selengkapnya</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="jumbotron slider" style="background-image: url(<?= base_url()?>asset/Jadi/bioskop2.png); background-size: cover; background-position: center;">
            <div class="container">
              <h1 style="color: white;">The Foreigner</h1>
              <p style="font-size: 14px; color: white; padding-right: 40%;"> The Foreigner menceritakan tentang seorang pengusaha sederhana bernama Quan (Jackie Chan) yang tinggal di London. Kehidupan bahagianya seketika berubah setelah sebuah kejadian terorisme berbau politik merenggut nyawa sang putri semata wayangnya, Fan (Katie Leung).</p>
              <p><a class="btn btn-primary btn-lg" role="button">Lihat Selengkapnya</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="jumbotron slider" style="background-image: url(<?= base_url()?>asset/Jadi/bioskop3.png); background-size: cover; background-position: center;">
            <div class="container">
              <h1 style="color: white;">Geostorm</h1>
              <p style="font-size: 14px; color: white; padding-right: 40%;">Geostorm akan menceritakan tentang seorang desainer. Dia adalah perancang satelit yang terkenal keras kepala, meski demikian, dia adalah orang yang cukup menawan. Namun suatu ketika terjadi sebuah kesalahan sistem penegndali iklim bumi dan mengalami kerusakan.</p>
              <p><a class="btn btn-primary btn-lg" role="button">Lihat Selengkapnya</a></p>
            </div>
          </div>      </div>
          <div class="item">
            <div class="jumbotron slider" style="background-image: url(<?= base_url()?>asset/Jadi/bioskop4.png); background-size: cover; background-position: center;">
              <div class="container">
                <h1 style="color: white;">Happy Death Day</h1>
                <p style="font-size: 14px; color: white; padding-right: 40%;">Happy Death Day adalah film yang menceritakan tentang kisah dari seorang mahasiswi bernama Tree Gelbman ( Jessica Rothe ) yang menghidupkan kembali hari pembunuhannya.  Di mana terdapat berbagai detail yang janggal hingga pada akhir yang mengerikan, dengan dari semua itu adalah agar dapat menemukan identitas dari sang pembunuh.</p>
                <p><a class="btn btn-primary btn-lg" role="button">Lihat Selengkapnya</a></p>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="jumbotron slider" style="background-image: url(<?= base_url()?>asset/Jadi/bioskop5.png); background-size: cover; background-position: center;">
              <div class="container">
                <h1 style="color: white;">SAW VIII : Jigsaw</h1>
                <p style="font-size: 14px; color: white; padding-right: 40%;">Film Jigsaw menceritakan tentang mayat-mayat yang kembali ditemukan disekitar kota dengan masing-masing tubuh mengalami kematian yang unik, luka bekas penyiksaan sadis. Seiring penyelidikan berlanjut, bukti tertuju kepada satu orang yang sesuai dengan apa yang pernah dilakukannya dimasa lalu, John Kramer. Akan tetapi bagaimana bisa terjadi?</p>
                <p><a class="btn btn-primary btn-lg" role="button">Lihat Selengkapnya</a></p>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="jumbotron slider" style="background-image: url(<?= base_url()?>asset/Jadi/bioskop6.png); background-size: cover; background-position: center;">
              <div class="container">
                <h1 style="color: white;">Thor : Ragnarok</h1>
                <p style="font-size: 14px; color: white; padding-right: 40%;">Film Thor Ragnarok berlatar empat tahun setelah kejadian di film Thor The Dark World dan dua tahun setelah kejadian di film Avengers Age of Ultron. Bercerita tentang kepulangan Thor ke Asgard setelah melawan Ultron karena mendengar adanya permasalahan yang terjadi ditempat asalnya tersebut.</p>
                <p><a class="btn btn-primary btn-lg" role="button">Lihat Selengkapnya</a></p>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="jumbotron slider" style="background-image: url(<?= base_url()?>asset/Jadi/bioskop7.png); background-size: cover; background-position: center;">
              <div class="container">
                <h1 style="color: white;">Beyond Skyline</h1>
                <p style="font-size: 14px; color: white; padding-right: 40%;">Film Beyond Skyline menceritakan tentang perjuangan seorang Detektif bernama Mark (Frank Grillo) yang berusaha untuk menyelamatkan anak laki-lakinya, Trent (Jonny Weston) yang diculik oleh alien jahat.</p>
                <p><a class="btn btn-primary btn-lg" role="button">Lihat Selengkapnya</a></p>
              </div>
            </div>      
          </div>
          <div class="item">
            <div class="jumbotron slider" style="background-image: url(<?= base_url()?>asset/Jadi/bioskop8.png); background-size: cover; background-position: center;">
              <div class="container">
                <h1 style="color: white;">Justice League</h1>
                <p style="font-size: 14px; color: white; padding-right: 40%;">Film Justice League akan bersetting beberapa bulan setelah kejadian difilm Batman vs Superman Dawn of Justice dengan melihat bagaimana Batman, setelah terinspirasi oleh pengorbanan Superman untuk umat manusia, mulai bekerja untuk mengumpulkan sekelompok metahumans dalam menghadapi seorang musuh besar, Steppenwolf dengan pasukan Parademons.</p>
                <p><a class="btn btn-primary btn-lg" role="button">Lihat Selengkapnya</a></p>
              </div>
            </div>
          </div>
          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div>
      </div>
      <div class="container-fluid play">
      <div class="row">
        <div class="col-md-12">
          <h1 style="text-align: center; color: white;">Now Playing</h1>
        </div>
      </div>
            <div class="col-sm-6 col-md-3">
              <div class="thumbnail">
                <a href="#" data-toggle="modal" data-target="#video1"><img src="<?= base_url()?>asset/jadi/bioskop1.png" alt="..." class="img-responsive"></a>
                <div class="caption">
                  <h3>Blade Runner 2049</h3>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3">
              <div class="thumbnail">
                <a href="#" data-toggle="modal" data-target="#video2"><img src="<?= base_url()?>asset/jadi/bioskop2.png" alt="..." class="img-responsive"></a>
                <div class="caption">
                  <h3>The Foreigner</h3>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3">
              <div class="thumbnail">
                <a href="#" data-toggle="modal" data-target="#video3"><img src="<?= base_url()?>asset/jadi/bioskop3.png" alt="..." class="img-responsive"></a>
                <div class="caption">
                  <h3>Geostorm</h3>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-3">
              <div class="thumbnail">
                <a href="#" data-toggle="modal" data-target="#video4"><img src="<?= base_url()?>asset/jadi/bioskop4.png" alt="..." class="img-responsive"></a>
                <div class="caption">
                  <h3>Happy Death Day</h3>
                </div>
              </div>
          </div>
        </div>
        <div class="container-fluid">
        <div class="row">
          <div class="jumbotron teater">
            <h1>Our Teater</h1>
            <h2>
              We have an International standart teater which have the best quality of seat that make you enjoy to watch movie.
              There also a cup holder in the seat so you dont worry where will you put your drink when watch the movie.
              Our studio supported by Dolby surround so it will bring the best sensation of the movie to you.
            </h2>
          </div>
        </div>
        </div>
        <div class="container-fluid support">
          <div class="row">
          <div class="col-md-12">
            <h1 style="text-align: center; color: black;">Supported by</h1>
          </div>
          </div>
          <div class="col-md-4 logo">
            <img src="<?= base_url()?>asset/jadi/4k_logo.png" class="img-responsive">
          </div>
          <div class="col-md-4 logo">
            <img src="<?= base_url()?>asset/jadi/dolby-logo.png" class="img-responsive">
          </div>
          <div class="col-md-4 logo">
            <img src="<?= base_url()?>asset/jadi/imax-logo.png" class="img-responsive">
          </div>
        </div>
        <div class="container-fluid location">
        <h1 style=" color: black;">Our Location</h1>
          <iframe src="<?= base_url()?>https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3168.639110340526!2d-122.08624618519327!3d37.42200414015089!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fba02425dad8f%3A0x6c296c66619367e0!2sGoogleplex!5e0!3m2!1sen!2sid!4v1510284588360" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <div class="container-fluid footer">
          <div class="col-md-4">
            <address>
              <strong>Deke Cinema</strong><br>
              785 Sutev Ave, Suite 600<br>
              Los Angeles, CA 12917<br>
              (435) 239-3819<br>
              <a href="<?= base_url()?>index.php/Bioskop/index">www.dekecinema.com<br>
              <a href="mailto:dekecinema@gmail.com">dekecinema@gmail.com</a>
            </address>
          </div>
          <div class="col-md-4">
            <ul>
              <li><a href="<?= base_url()?>index.php/Bioskop/index">Home</li>
              <li><a href="<?= base_url()?>index.php/Bioskop/play">Now Playing</li>
              <li><a href="<?= base_url()?>index.php/Bioskop/transaksi">Book Ticket</a></li>
              <li><a href="<?= base_url()?>index.php/Bioskop/kritik">Kritik dan Saran</li>
            </ul>
          </div>
          <div class="col-md-4">
            <div class="input-group">
              <input type="text" class="form-control">
              <span class="input-group-btn">
              <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
              </span>
            </div>
            <h2>Find Us On</h2>
            <a href="https://www.facebook.com/andhika.miftaalauddin" target="new"><img src="<?= base_url()?>asset/jadi/facebook-icon-circle.png"></a>
            <a href="https://www.instagram.com/andhikamifta/" target="new"><img src="<?= base_url()?>asset/jadi/social-instagram-new-circle-512.png"></a>
            <a href="https://plus.google.com/u/0/101217493818455086727" target="new"><img src="<?= base_url()?>asset/jadi/social-circle-google-plus-2-128.png"></a>
          </div>
        </div>


        <!--Modal-->

        <div class="modal fade bs-example-modal-sm">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Attention</h4>
              </div>
              <div class="modal-body">
              <div class="alert alert-danger" role="alert">Page under maintenance</div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="video1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Blade Runner 2049</h4>
      </div>
      <div class="modal-body">
        <center><iframe src="<?= base_url()?>asset/Jadi/captain tsubasa opening indonesia.mp4" width="330" height="250">
        </iframe></center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="video2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">The Foreigner</h4>
      </div>
      <div class="modal-body">
        <center><iframe src="<?= base_url()?>asset/Jadi/captain tsubasa opening indonesia.mp4" width="330" height="250">
        </iframe></center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="video3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Geostorm</h4>
      </div>
      <div class="modal-body">
        <center><iframe src="<?= base_url()?>asset/Jadi/captain tsubasa opening indonesia.mp4" width="330" height="250">
        </iframe></center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="video4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Happy Death Day</h4>
      </div>
      <div class="modal-body">
        <center><iframe src="<?= base_url()?>asset/Jadi/captain tsubasa opening indonesia.mp4" width="330" height="250">
        </iframe></center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </div>
    </div>
  </div>

    </body>
    </html>