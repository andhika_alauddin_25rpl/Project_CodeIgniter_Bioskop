<div class="container-fluid footer">
          <div class="col-md-4">
            <address>
              <strong>Deke Cinema</strong><br>
              785 Sutev Ave, Suite 600<br>
              Los Angeles, CA 12917<br>
              (435) 239-3819<br>
              <a href="<?= base_url()?>index.php/Bioskop/index">www.dekecinema.com<br>
              <a href="mailto:dekecinema@gmail.com">dekecinema@gmail.com</a>
            </address>
          </div>
          <div class="col-md-4">
            <ul>
              <li>Home</li>
              <li>Now Playing</li>
              <li>Book Ticket</li>
              <li>About Us</li>
            </ul>
          </div>
          <div class="col-md-4">
            <div class="input-group">
              <input type="text" class="form-control">
              <span class="input-group-btn">
              <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
              </span>
            </div>
            <h2>Find Us On</h2>
            <a href="https://www.facebook.com/andhika.miftaalauddin" target="new"><img src="<?= base_url()?>asset/jadi/facebook-icon-circle.png"></a>
            <a href="https://www.instagram.com/andhikamifta/" target="new"><img src="<?= base_url()?>asset/jadi/social-instagram-new-circle-512.png"></a>
            <a href="https://plus.google.com/u/0/101217493818455086727" target="new"><img src="<?= base_url()?>asset/jadi/social-circle-google-plus-2-128.png"></a>
          </div>
        </div>

    </body>
    </html>