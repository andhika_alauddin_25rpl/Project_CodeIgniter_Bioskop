<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url()?>asset/bootstrap-3.3.7-dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url()?>asset/style.css">
  <link rel="icon" type="img" href="<?= base_url()?>asset/jadi/icon.png">
  	<script type="text/javascript" src="<?= base_url()?>asset/jquery-ui-1.12.1/external/jquery/jquery.js"></script>
  	<script type="text/javascript" src="<?= base_url()?>asset/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
  	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<style type="text/css">
.col-md-6 h4{
	padding-top: 10vh;
}
.form{
	background-color: #181919; 
	height: 100vh;
	padding-top: 20vh;
}
.form .input-group{
	padding-bottom: 2vh;
}
.login{
	float: right;
}
</style>
<body>

<nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
     <div class="navbar-header">

       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
       </button>
       <a class="navbar-brand" href="<?= base_url()?>/index.php/Bioskop/index"><img src="<?= base_url()?>asset/jadi/logo.png"></a>
     </div>
     <div class="container-fluid">
       <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <ul class="nav navbar-nav">
           <li><a href="<?= base_url()?>index.php/Bioskop/play">Now Playing</a></li>
           <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm">About Us</a></li>
         </ul>
         <ul class="nav navbar-nav navbar-right">
          <li><a href="<?= base_url()?>index.php/Bioskop/login"><span class="glyphicon glyphicon-log-in">&nbsp</span>Login</a></li>
          <li><a href="<?= base_url()?>index.php/Bioskop/register">Daftar</a></li>
        </ul>
      </div>
    </div>
  </nav>

<div class="container-fluid">
	<div class="col-md-6 container-fluid">
		<h4 align="center"><b>ENJOY THE NEWEST MOVIE HERE<br>SIGN UP NOW TO GET FREE 3 TICKET AND 2 COCA-COLA 16oz*</h4>
		<img src="<?= base_url()?>asset/jadi/login.png" class="img-responsive">
	</div>
	<div class="col-md-6 form container-fluid">
		<div class="panel panel-info">
          <?php
    if ($this->session->flashdata('pesan')!=null) {
      echo "
    <div class='alert alert-danger'>".$this->session->flashdata("pesan")."</div>";
  }
  ?>
  			<div class="panel-heading">
    		<h3 class="panel-title">Sign Up</h3>
  			</div>
        <form action="<?= base_url('index.php/bioskop/simpan')?>" method="post">
    		<div class="panel-body">
          <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
          <input name="name" type="text" class="form-control" placeholder="Name">
        </div>
          <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
          <input name="email" type="email" class="form-control" placeholder="Email">
        </div>
    			<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
				  <input name="username" type="text" class="form-control" placeholder="Username">
				</div>
    			<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
				  <input name="password" type="password" class="form-control" placeholder="Password">
				</div>
          <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
          <input name="retype" type="password" class="form-control" placeholder="Re-type Password">
        </div>
				<div class="login">
					<input type="submit" name="proses" value="Daftar" class="btn btn-primary">
				</div>
  			</div>
        </form>
  			<div class="panel-footer" align="center">Already have an account? <a href="login.html">Login</a> Now</div>
		</div>
	</div>
</div>

        <!--Modal-->

        <div class="modal fade bs-example-modal-sm">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Attention</h4>
              </div>
              <div class="modal-body">
              <div class="alert alert-danger" role="alert">Page under maintenance</div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade bs-example-modal-sm" id="video1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Attention</h4>
              </div>
              <div class="modal-body">
              <div class="alert alert-success" role="alert">Sign Up Success</div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

</body>
</html>
