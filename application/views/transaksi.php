<div class="container-fluid konten">
<div class="col-md-6 form container-fluid">
		<div class="panel panel-success">
  			<div class="panel-heading">
    		<h3 class="panel-title">Book Ticket</h3>
  			</div>
  			<div class="panel-body">
    			<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
				  <input type="text" class="form-control" placeholder="Nama">
				</div>
				<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
				  <input type="email" class="form-control" placeholder="Email">
				</div>
				<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span></span>
				  <input type="tel" class="form-control" placeholder="No. Telepon">
				</div>
    			<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-film"></span></span>
				  <select class="form-control">
				  	<option></option>
				  	<option class="form-control">Thor : Ragnarok</option>
				  	<option>Yowis Ben</option>
				  	<option>Si Juki the Movie</option>
				  </select>
				</div>
				<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
				  <input type="number" class="form-control" placeholder="Pilih Jam Tayang">
				</div>
				<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
				  <input type="date" class="form-control" placeholder="Pilih Tanggal">
				</div>
				<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
				  <input type="number" class="form-control" placeholder="Masukkan Jumlah Tiket">
				</div>
				<div class="login">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#book">BOOK</button>
				</div>
  			</div>
  			<div class="panel-footer" align="center"></div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-sm" id="book" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Attention</h4>
              </div>
              <div class="modal-body">
              <div class="alert alert-success" role="alert">Book Success</div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->