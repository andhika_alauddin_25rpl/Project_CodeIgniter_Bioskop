<div class="container-fluid konten">
<div class="col-md-6 form container-fluid">
    <div class="panel panel-info">
        <div class="panel-heading">
        <h3 class="panel-title">Kritik dan Saran</h3>
        </div>
        <div class="panel-body">
          <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
          <input type="text" class="form-control" placeholder="Nama">
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
          <input type="text" class="form-control" placeholder="Email">
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span></span>
          <input type="text" class="form-control" placeholder="No. Telephone">
        </div>
        <div class="input-group">
          <span class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span></span>
          <input type="textarea" class="form-control" placeholder="Kritik dan Saran">
        </div>
        <div class="login">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#video1">Kirim</button>
        </div>
        </div>
        <div class="panel-footer" align="center"></div>
    </div>
  </div>

<div class="modal fade bs-example-modal-sm" id="video1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Attention</h4>
              </div>
              <div class="modal-body">
              <div class="alert alert-success" role="alert">Login Success</div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
</div>