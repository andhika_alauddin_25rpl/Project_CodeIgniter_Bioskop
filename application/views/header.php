  <!DOCTYPE html>
  <html>
  <head>
  	<title><?= $judul?></title>
  	<link rel="stylesheet" type="text/css" href="<?= base_url()?>asset/bootstrap-3.3.7-dist/css/bootstrap.css">
  	<link rel="stylesheet" type="text/css" href="<?= base_url()?>asset/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>asset/play.css">
    <link rel="icon" type="img" href="<?= base_url()?>asset/jadi/icon.png">
  	<script type="text/javascript" src="<?= base_url()?>asset/jquery-ui-1.12.1/external/jquery/jquery.js"></script>
  	<script type="text/javascript" src="<?= base_url()?>asset/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>

    <nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
     <div class="navbar-header">

       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
       </button>
       <a class="navbar-brand" href="<?= base_url()?>index.php/Bioskop/index"><img src="<?= base_url()?>asset/jadi/logo.png"></a>
     </div>
     <div class="container-fluid">
       <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <ul class="nav navbar-nav">
           <li><a href="<?= base_url()?>index.php/Bioskop/play">Now Playing</a></li>
           <li><a href="#" data-toggle="modal" data-target=".bs-example-modal-sm">About Us</a></li>
         </ul>
         <ul class="nav navbar-nav navbar-right">
          <li><a href="<?= base_url()?>index.php/Bioskop/login"><span class="glyphicon glyphicon-log-in">&nbsp</span>Login</a></li>
          <li><a href="<?= base_url()?>index.php/Bioskop/register">Daftar</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <?php 
    $this->load->view($konten);
  ?>
  <div class="container-fluid footer">
          <div class="col-md-4">
            <address>
              <strong>Deke Cinema</strong><br>
              785 Sutev Ave, Suite 600<br>
              Los Angeles, CA 12917<br>
              (435) 239-3819<br>
              <a href="<?= base_url()?>index.php/Bioskop/index">www.dekecinema.com<br>
              <a href="mailto:dekecinema@gmail.com">dekecinema@gmail.com</a>
            </address>
          </div>
          <div class="col-md-4">
            <ul>
              <li><a href="<?= base_url()?>index.php/Bioskop/index">Home</li>
              <li><a href="<?= base_url()?>index.php/Bioskop/play">Now Playing</li>
              <li><a href="<?= base_url()?>index.php/Bioskop/transaksi">Book Ticket</a></li>
              <li><a href="<?= base_url()?>index.php/Bioskop/kritik">Kritik dan Saran</li>
            </ul>
          </div>
          <div class="col-md-4">
            <div class="input-group">
              <input type="text" class="form-control">
              <span class="input-group-btn">
              <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
              </span>
            </div>
            <h2>Find Us On</h2>
            <a href="https://www.facebook.com/andhika.miftaalauddin" target="new"><img src="<?= base_url()?>asset/jadi/facebook-icon-circle.png"></a>
            <a href="https://www.instagram.com/andhikamifta/" target="new"><img src="<?= base_url()?>asset/jadi/social-instagram-new-circle-512.png"></a>
            <a href="https://plus.google.com/u/0/101217493818455086727" target="new"><img src="<?= base_url()?>asset/jadi/social-circle-google-plus-2-128.png"></a>
          </div>
        </div>

    </body>
    </html>