<div class="container-fluid konten">
  <div class="row">
    <div class="col-md-12 judul">
      <h1>Now Playing</h1>
    </div>
  </div>
  <div class="row">
    <table class="table table-bordered tabel">
      <tr>
        <th style="width: 40%;">Judul Film</th>
        <th style="width: 40%;">Jam Tayang</th>
        <th>Opsi</th>
      </tr>
      <?php
        foreach ($film as $data_film) {
        ?>
      <tr>
        <td rowspan="5"><a href="<?=base_url('index.php/Bioskop/detail_film/'.$data_film->id_film)?>"><img src="<?= base_url()?>asset/jadi/<?= $data_film->foto_film;?>"></a></td>
        <td>10.00</td>
        <td>
          <a href="<?= base_url()?>index.php/Bioskop/transaksi"><button type="button" class="btn btn-success beli">Beli Tiket</button></a>
          <button type="button" class="btn btn-info beli">Lihat Trailer</button>
        </td>
      </tr>
      <tr>
        <td>12.45</td>
        <td>
          <a href="<?= base_url()?>index.php/Bioskop/transaksi"><button type="button" class="btn btn-success beli">Beli Tiket</button></a>
          <button type="button" class="btn btn-info beli">Lihat Trailer</button>
        </td>
      </tr>
      <tr>
        <td>15.05</td>
        <td>
          <a href="<?= base_url()?>index.php/Bioskop/transaksi"><button type="button" class="btn btn-success beli">Beli Tiket</button></a>
          <button type="button" class="btn btn-info beli">Lihat Trailer</button>
        </td>
      </tr>
      <tr>
        <td>17.20</td>
        <td>
          <a href="<?= base_url()?>index.php/Bioskop/transaksi"><button type="button" class="btn btn-success beli">Beli Tiket</button></a>
          <button type="button" class="btn btn-info beli">Lihat Trailer</button>
        </td>
      </tr>
      <tr>
        <td>19.35</td>
        <td>
          <a href="<?= base_url()?>index.php/Bioskop/transaksi"><button type="button" class="btn btn-success beli">Beli Tiket</button></a>
          <button type="button" class="btn btn-info beli">Lihat Trailer</button>
        </td>
      </tr>
      <?php } ?>
    </table>
  </div>
</div>